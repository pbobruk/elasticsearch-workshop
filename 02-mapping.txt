DELETE products

PUT products
{
  "mappings": {
    "properties": {
      "name": { "type": "text", "analyzer": "simple"},
      "name_original": { "type": "text", "analyzer": "keyword"},
      "category": { "type": "text", "index": false}
    }
  }
}

GET /products/_mapping

PUT products/_mapping
{
  "properties": {
    "name2": {
      "type": "text",
      "analyzer": "keyword"
    }
  }
}

GET /products/_mapping

GET products/_analyze
{
  "field" : "name",
  "text" : "The Quick Fox Jumped"
}

GET products/_analyze
{
  "field" : "name_original",
  "text" : "The Quick Fox Jumped"
}

# Zdefiniuj index products2, tak aby analizator pola name usuwał "stopwordy".
